﻿module qlib;

import std.stdio;
import std.conv:to;
import core.runtime;

union Tqvar{
	double d;
	string s;
	Tqvar[] array;
}

export shared static this(){
	rt_init;
}
export shared static ~this(){
	rt_term;
}

export extern(C){
	//Required by QCode
	string[] funcs(){
		return ["qwrite","qread"];
	}
	//Console I/O
	Tqvar qwrite(Tqvar[] args){
		foreach(arg;args){
			write(arg.s);
		}
		return args[0];
	}
	Tqvar qread(Tqvar[] args){
		Tqvar r;
		r.s = readln;
		r.length--;//To remove the newline char
		return r;
	}
}