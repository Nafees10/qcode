﻿module qcode;

import misc;
import lists;
import qloader;
import std.stdio;
import std.string;
//<FOR DYNLIBS>
import core.stdc.stdio;
import core.stdc.stdlib;
import core.sys.posix.dlfcn;
//</FOR DYNLIBS>

alias scrFunction = Tqvar delegate(Tqvar[] args);
alias libFunction = extern(C) Tqvar function(Tqvar[]);

class Tqcode{
private:
	Tqloader qscr;

	string libDir;//stores the dir of the libs
	libFunction[string] plist;//To store functions' pointers loaded from libs
	void*[string] libList;//self-explanatary

	Tqvar loadLib(Tqvar[] args){
		string[] fList;
		char* error;
		foreach(curLib; args){
			libList[curLib.s] = dlopen(
				cast(const char*)toStringz(libDir~"/"~curLib.s~".so"),RTLD_LAZY);

			if (!libList[curLib.s]){
				throw new Exception("failed to load library "~curLib.s);
			}

			fList = (cast(string[] function())dlsym(libList[curLib.s],"funcs"))();
			foreach(fname; fList){
				plist[fname] = cast(libFunction)dlsym(libList[curLib.s],
					cast(const char*)toStringz(fname));

				error = dlerror();

				if (error){
					core.stdc.stdio.fprintf(core.stdc.stdio.stderr, "dlsym error: %s\n",
						error);
				}
			}
		}
		return args[0];
	}

public:
	this(string lDir){
		libDir = lDir;
		qscr = new Tqloader();
		qscr.setExec(&call);
	}

	string[] loadScript(string fname){
		return qscr.loadScript(fname);
	}
	
	Tqvar call(string name, Tqvar[] args){
		libFunction* f;
		Tqvar r;
		if (name!="import"){
			f = name in plist;
			if (f){
				r = (*f)(args);
			}else{
				writeln("ERROR# Unknown function: ",name);
			}
		}else{
			r = loadLib(args);
		}
		return r;
	}

	~this(){
		delete qscr;
		//Free all the loaded libraries!
		foreach(lib; libList){
			dlclose(lib);
		}
	}
	void execute(string func){
		qscr.execute(func);
	}
}