﻿module main;

import misc;
import qcode;
import std.path;
import std.file;
import std.stdio;
import core.memory;

void main(string[] args){
	GC.disable;
	if (args.length<2 || args[1]=="-help"){
		writeln("not enough arguments");
		writeln("first argument should be name of script file to execute.");
		writeln("second [default=main] should be name of function to execute.");
	}else{
		string fName = getcwd~"/"~args[1];
		if (exists(fName)){
			Tqcode qc = new Tqcode("/usr/share/qcode");
			string[] errors = qc.loadScript(fName);
			
			if (errors.length>0){
				writeln("there are errors:");
				foreach(err; errors){
					writeln(err);
				}
			}else{
				if (args.length>2){
					qc.execute(args[2]);
				}else{
					qc.execute("main");
				}
			}
		}
	}
}

