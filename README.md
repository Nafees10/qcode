README

QCode is an extend-able runner for QScript scripts. This repo contains
the source code to QCode, and an extension, QLib. Going through QLib's
source will explain how to write an extension for QCode.

Everyone is welcome to contribute to it. If you want to contribute,
please do so in the dev branch.

Compiling it:
You will need the QScript2 shared library, rename it to qscript2.so,
place it in /usr/share/qcode, then compile and run qcode with the first
argument the name/path+name of the script to execute, second argument
is the name of function to start execution from, default is main.

Contact:
Nafees Hassan: nafees [dot] hassan [at] outlook [dot] com